let papaparse = require('papaparse')
let fs = require('fs')

let matchesdata = fs.readFileSync("/home/sachin/project/ipl/src/data/matches.csv", 'utf-8')
let deliveriesdata = fs.readFileSync("/home/sachin/project/ipl/src/data/deliveries.csv", 'utf-8')


const matches = papaparse.parse(matchesdata, {
    header: true
})
const deliveries = papaparse.parse(deliveriesdata, {
    header: true
})

const matchesData = matches["data"];
const deliveriesData = deliveries["data"]


// 1.

function playedMatch(matchesData) {
    if (!Array.isArray(matchesData)) {
        return {}
    }
    let playedMatch = matchesData.reduce(function (output, current) {
        if (output[current.season])
            output[current.season] += 1;
        else
            output[current.season] = 1;
        return output;
    }, {});
    return playedMatch;
}

let solution1 = playedMatch(matchesData);


fs.writeFileSync("/home/sachin/project/ipl/src/public/output/matchesPlayedPerYear.json", JSON.stringify(solution1));


// 2.
function wonPerTeam(matchesData) {
    if (!Array.isArray(matchesData)) {
        return {};
    }

    let reducer = matchesData.reduce((year, match) => {
        if (year[match.season] === undefined) {
            year[match.season] = {};
        }
        if (year[match.season][match.winner] === undefined) {
            year[match.season][match.winner] = 1;
        }
        else {
            year[match.season][match.winner] += 1;
        }

        return year;

    }, {});
    return reducer;
}

let solution2 = wonPerTeam(matchesData);


fs.writeFileSync("/home/sachin/project/ipl/src/public/output/matchesWonPerTeam.json", JSON.stringify(solution2));


// 3.

const extraRunPerTeam2016 = (deliveryData, matchData) => {
    if (!deliveryData && !matchData) return {};

    function getSeason16(data) {
        if (data.season === "2016") {
            return data['id'];
        }
    }

    let season16ids = matchData.filter(getSeason16).map(ids => parseInt(ids.id));


    let extraRuns = deliveryData.filter(data => season16ids.includes(parseInt(data.match_id)))
        .reduce((extraRun, delivery) => {
            let bowlingTeam = delivery["bowling_team"]
            if (extraRun[bowlingTeam]) {
                extraRun[bowlingTeam] += parseInt(delivery["extra_runs"]);
            }
            else {
                extraRun[bowlingTeam] = parseInt(delivery["extra_runs"]);
            }
            return extraRun;
        }, {});
    return extraRuns;
}

let solution3 = extraRunPerTeam2016(deliveriesData, matchesData);

fs.writeFileSync("/home/sachin/project/ipl/src/public/output/extraRunPerTeam2016.json", JSON.stringify(solution3))


// 4.


function topTenEconomicalBowlers2015(dataDelivery, dataMatch) {

    let ids = dataMatch.filter(obj => obj.season === '2015').map(obj => parseInt(obj.id));

    let deliveries2015 = dataDelivery.filter(val => ids.includes(parseInt(val.match_id)));

    let totalRunsBalls = deliveries2015.reduce((output, delivery) => {
        if (!output[delivery.bowler]) {
            output[delivery.bowler] = {};
            output[delivery.bowler].balls = 1;
            output[delivery.bowler].over = 1;
            output[delivery.bowler].concededRun = parseInt(delivery.total_runs);
            output[delivery.bowler].economyRate = +((output[delivery.bowler].concededRun / ((output[delivery.bowler].balls) / 6)).toFixed(3));
        }
        else {
            output[delivery.bowler].balls += 1;
            output[delivery.bowler].over = parseInt(output[delivery.bowler].balls / 6);
            output[delivery.bowler].concededRun += parseInt(delivery.total_runs);
            output[delivery.bowler].economyRate = +((output[delivery.bowler].concededRun / ((output[delivery.bowler].balls) / 6)).toFixed(3));
        }
        return output;
    }, {});

    let topTenEconomicalBowler = Object.entries(totalRunsBalls).sort((a, b) => a[1].economyRate - b[1].economyRate).slice(0, 10);

    return topTenEconomicalBowler;
}

let solution4 = topTenEconomicalBowlers2015(deliveriesData, matchesData);

fs.writeFileSync("/home/sachin/project/ipl/src/public/output/top10EconomialBowler.json", JSON.stringify(solution4));




// E1

let wonMatchAndToss = matchesData.reduce(function (matchdetail, current) {
    if (!current.id == '') {
        if (matchdetail[current.toss_winner] == matchdetail[current.winner]) {
            if (matchdetail[current.toss_winner]) {
                matchdetail[current.toss_winner] += 1;
            } else {
                matchdetail[current.toss_winner] = 1;
            }
        }
    }
    return matchdetail;
}, {});

fs.writeFileSync("/home/sachin/project/ipl/src/public/output/wonMatchAndToss.json", JSON.stringify(wonMatchAndToss));


// E2

let playerOfMatchPerSeason = matchesData.reduce(function (playerOfMatch, current) {
    if (!current.id == " ") {

        if (playerOfMatch[current.season]) {

            if (playerOfMatch[current.season][current.player_of_match]) {
                playerOfMatch[current.season][current.player_of_match]++
            }
            else {
                playerOfMatch[current.season][current.player_of_match] = 1
            }

        }
        else {
            playerOfMatch[current.season] = {}

        }
    }
    return playerOfMatch

}, {})

let playerOfMatch = Object.entries(playerOfMatchPerSeason)
let total = playerOfMatch.reduce((acc, curr) => {

    let sortedPlayer = Object.entries(curr[1]).sort((a, b) => {

        return b[1] - a[1];
    }).slice(0, 1);

    acc[curr[0]] = Object.fromEntries(sortedPlayer)
    return acc;

}, {})

fs.writeFileSync("/home/sachin/project/ipl/src/public/output/playerOfMatchPerSeason.json",JSON.stringify(total));



// E4

let playerDismissedByAnotherPlayer=(function(deliveriesData) {

    let dismissed = deliveriesData.reduce((output, curr) => {

        if (curr.match_id != '') {

            if ((curr.player_dismissed !== '') && (curr.dismissal_kind !== 'run out')) {

                if (output.hasOwnProperty(curr.batsman)) {

                    if (output[curr.batsman]['dismissed_by'] === curr.bowler) {

                        output[curr.batsman]['dismissals']++;
                    }
                } else {

                    output[curr.batsman] = {};
                    output[curr.batsman]['dismissed_by'] = curr.bowler;
                    output[curr.batsman]['dismissals'] = 1
                }
            }
        }

        return output;

    }, {});

    let datadismissed = Object.fromEntries(Object.entries(dismissed).sort((val1, val2) => {

        if (val1[1].dismissals > val2[1].dismissals) {
            return -1;
        } else {
            return 1;
        }
    }).slice(0, 3));

    return datadismissed;

})

fs.writeFileSync("/home/sachin/project/ipl/src/public/output/highestTimeDismissedByOther.json",JSON.stringify(playerDismissedByAnotherPlayer(deliveriesData)));



// E5
let bowlerBestEconomySuperOvers = function (dataDelivery) {

    let dataBowler = dataDelivery.reduce((output, current) => {
        if (current.match_id != '') {
            if (current.is_super_over != 0) {
                if (output[current.bowler]) {
                    output[current.bowler]['scores'] += parseInt(current.total_runs);
                    output[current.bowler]['balls'] ++ ;
                    output[current.bowler]['economy'] = parseFloat((output[current.bowler]['scores'] / ((output[current.bowler]['balls']) / 6)).toFixed(2));

                } else {
                    output[current.bowler] = {};
                    output[current.bowler]['scores'] = parseInt(current.total_runs);
                    output[current.bowler]['balls'] = 1;
                }
            }
        }
        return output;
    }, {});
    let topBowlerData = Object.fromEntries(Object.entries(dataBowler).sort((val1, val2) => {
        if (val1[1].economy > val2[1].economy) {
            return 1;
        } else {
            return -1;
        }
    }).slice(0,1));
    return topBowlerData;
}
let result = bowlerBestEconomySuperOvers(deliveriesData);
console.log(result);

fs.writeFileSync("/home/sachin/project/ipl/src/public/output/bowlerBestEconomyInSuperOvers.json",JSON.stringify(result));

// E3

function deliveriesPerYear(matchesData, deliveriesData, year) {
    
    let ids = matchesData.filter(obj => obj.season == year)
    .map(obj => parseInt(obj.id));

    return deliveriesData.filter(val => ids.includes(parseInt(val.match_id)));
}
let playerName = "Yuvraj Singh";
function strikeRateEachYear(matchesData, deliveriesData, years, playerName) {

    let strikeRate = deliveriesPerYear(matchesData, deliveriesData, years)
    .reduce((output, current) => {

        if (current.batsman === playerName) {

            if (output[current.batsman]) {

                output[current.batsman]["runs"] += parseInt(current.batsman_runs);
                output[current.batsman]["balls"] += 1;
                output[current.batsman]["year"] = years;
            }
            else {

                output[current.batsman] = {};
                output[current.batsman]["runs"] = parseInt(current.batsman_runs);
                output[current.batsman]["balls"] = 1;
            }

            output[current.batsman]["strikeRate"] = parseFloat((parseInt(output[current.batsman]["runs"]) * 100 / parseInt(output[current.batsman]["balls"])).toFixed(2));

        }

        return output;

    }, {});
    
    return strikeRate;
}
let years = [2008,2009,2010,2011,2012,2013,2014,2015,2016,2017]

let result1 = years.map((year) => {
    return (strikeRateEachYear(matchesData, deliveriesData, year, playerName))
});

fs.writeFileSync("/home/sachin/project/ipl/src/public/output/batsmanStrikeRateForEachSeason.json3",JSON.stringify(result1));
